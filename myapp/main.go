package main

import (
	"recipelab/myapp/routes"
)

func main() {

	routes.IntializeRoutes()
}
