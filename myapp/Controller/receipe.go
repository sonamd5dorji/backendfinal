package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	model "recipelab/myapp/Model"
	"recipelab/myapp/utils/httpResp"
	"strconv"

	"github.com/gorilla/mux"
)

func converter(value string) (int64, error) {
	rid, err := strconv.ParseInt(value, 10, 64)
	return rid, err
}

func AddRecipe(w http.ResponseWriter, r *http.Request) {
	cookie, _ := r.Cookie("my-cookie")

	recipe := model.Recipe{CID: cookie.Value}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&recipe)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid JSON body.")
		return
	}

	recipeAddErr := recipe.AddRecipe()

	fmt.Println(recipe)

	if recipeAddErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, recipeAddErr.Error())
	}
	httpResp.ResponseWithJSON(w, http.StatusOK, map[string]model.Recipe{"status": recipe})
}

func GetRecipe(w http.ResponseWriter, r *http.Request) {
	// if !VerifyCookie(w, r) {
	// 	return
	// }

	rid := mux.Vars(r)["rid"]
	ridNum, _ := converter(rid)

	recipe := model.Recipe{RecipeID: int(ridNum)}

	getErr := recipe.GetRecipe()

	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Recipe not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResp.ResponseWithJSON(w, http.StatusOK, recipe)
	}
}

func GetAllRecipe(w http.ResponseWriter, r *http.Request) {
	// if !VerifyCookie(w, r) {
	// 	return
	// }

	cookie, _ := r.Cookie("my-cookie")
	email := cookie.Value

	recipes, getErr := model.GetAllRecipe(email)

	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.ResponseWithJSON(w, http.StatusOK, recipes)
}

func DeleteRecipe(w http.ResponseWriter, r *http.Request) {
	// if !VerifyCookie(w, r) {
	// 	return
	// }

	rid := mux.Vars(r)["rid"]
	ridNum, _ := converter(rid)

	recipe := model.Recipe{RecipeID: int(ridNum)}

	err := recipe.Delete()

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.ResponseWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func UpdateRecipe(w http.ResponseWriter, r *http.Request) {
	// if !VerifyCookie(w, r) {
	// 	return
	// }

	rid := mux.Vars(r)["rid"]
	ridNum, _ := converter(rid)

	var recipe model.Recipe

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&recipe)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json")
		return
	}

	UpdateErr := recipe.Update(int(ridNum))

	if UpdateErr != nil {
		switch UpdateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Recipe not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, UpdateErr.Error())
		}
	} else {
		httpResp.ResponseWithJSON(w, http.StatusOK, recipe)
	}
}
