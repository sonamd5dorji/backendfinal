package controller_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	controller "recipelab/myapp/Controller"
	model "recipelab/myapp/Model"
	"testing"
	"time"
)

func TestAdmLogin(t *testing.T) {
	url := "http://localhost:8080/login"

	var jsonStr = []byte(`{"cid":"1234", "password":"aaaa"}`)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"message": "login success"}`

	assert.JSONEq(t, expResp, string(body))
}

func TestSignup_Success(t *testing.T) {
	admin := model.Admin{
		// Set admin fields here
	}

	payload, _ := json.Marshal(admin)
	req, err := http.NewRequest("POST", "/signup", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.Signup)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Signup handler returned wrong status code: got %v, want %v", status, http.StatusCreated)
	}

	expected := `{"status":"admin added"}`
	if rr.Body.String() != expected {
		t.Errorf("Signup handler returned unexpected body: got %v, want %v", rr.Body.String(), expected)
	}
}

func TestSignup_InvalidJSON(t *testing.T) {
	payload := []byte(`{"invalid":"json"}`) // Invalid JSON payload
	req, err := http.NewRequest("POST", "/signup", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.Signup)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("Signup handler returned wrong status code: got %v, want %v", status, http.StatusBadRequest)
	}

	expected := `{"error":"invalid json body"}`
	if rr.Body.String() != expected {
		t.Errorf("Signup handler returned unexpected body: got %v, want %v", rr.Body.String(), expected)
	}
}

func TestLogin_Success(t *testing.T) {
	admin := model.Admin{
		// Set admin fields here
	}

	payload, _ := json.Marshal(admin)
	req, err := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.Login)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Login handler returned wrong status code: got %v, want %v", status, http.StatusOK)
	}

	expected := `{"message":"success"}`
	if rr.Body.String() != expected {
		t.Errorf("Login handler returned unexpected body: got %v, want %v", rr.Body.String(), expected)
	}

	// Check if the cookie was set correctly
	cookie := rr.Result().Cookies()[0]
	if cookie.Name != "my-cookie" {
		t.Errorf("Login handler didn't set the correct cookie name: got %v, want %v", cookie.Name, "my-cookie")
	}
	if cookie.Value != "my-value" {
		t.Errorf("Login handler didn't set the correct cookie value: got %v, want %v", cookie.Value, "my-value")
	}
	if cookie.Expires.Before(time.Now().Add(30 * time.Minute)) {
		t.Errorf("Login handler didn't set the correct cookie expiration time")
	}
	if !cookie.Secure {
		t.Errorf("Login handler didn't set the cookie as secure")
	}
}

func TestLogin_InvalidJSON(t *testing.T) {
	payload := []byte(`{"invalid":"json"}`) // Invalid JSON payload
	req, err := http.NewRequest("POST", "/login", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.Login)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("Login handler returned wrong status code: got %v, want %v", status, http.StatusBadRequest)
	}

	expected := `{"error":"Invalid json body"}`
	if rr.Body.String() != expected {
		t.Errorf("Login handler returned unexpected body: got %v, want %v", rr.Body.String(), expected)
	}
}
