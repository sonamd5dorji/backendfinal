package routes

import (
	"log"
	"net/http"
	controller "recipelab/myapp/Controller"

	"github.com/gorilla/mux"
)

func IntializeRoutes() {
	router := mux.NewRouter()
	//user
	router.HandleFunc("/signup", controller.SignUp).Methods("POST")
	router.HandleFunc("/admin", controller.GetAdmin).Methods("GET")

	router.HandleFunc("/admin/{cid}", controller.UpdateProfile).Methods("PUT")
	router.HandleFunc("/login", controller.Login).Methods("POST")

	router.HandleFunc("/totaladmins", controller.GetTotalAdmin).Methods("GET")
	router.HandleFunc("/recipe/add", controller.AddRecipe).Methods("POST")
	// RECIPE ROUTE
	router.HandleFunc("/recipe/add", controller.AddRecipe).Methods("POST")
	router.HandleFunc("/recipe/{rid}", controller.GetRecipe).Methods("GET")
	router.HandleFunc("/recipes", controller.GetAllRecipe).Methods("GET")
	router.HandleFunc("/recipe/delete/{rid}", controller.DeleteRecipe).Methods("DELETE")
	router.HandleFunc("/recipe/update/{rid}", controller.UpdateRecipe).Methods("PUT")

	fhandler := http.FileServer(http.Dir("./Index"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 2023...")
	log.Fatal(http.ListenAndServe(":2023", router))

}
