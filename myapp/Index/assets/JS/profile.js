
window.onload = function() {
    getUserProfile()
}
var cid = document.getElementById('cid')
var firstName = document.getElementById('firstname')
var lastName = document.getElementById("lastname")


function getUserProfile() {
    fetch('/admin')
    .then(response => response.text())
    .then(data => showProfile(data))
    .catch((error) => {
        alert(error)
    })
}
function showProfile(data){
    const admin = JSON.parse(data)
    showValue(admin)
}
function showValue(admin){ 
    cid.value = admin.cid
    firstName.value = admin.fname
    lastName.value = admin.lname
}
  


function updateProfile() {
  var updatedAdmin = {
      cid: document.getElementById("cid").value,
      fname: document.getElementById("firstname").value,
      lname: document.getElementById("lastname").value,
  }

  var CID = document.getElementById('cid').value

  console.log(updatedAdmin);
  console.log(CID);

  fetch(`/admin/${CID}`, {
      method: 'PUT',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(updatedAdmin)
  })
  .then(response => response.text())
  .then(data => {
      // Handle the response if needed
      console.log("data" , data)
  })
  .catch((error) => {
      console.log(error)
  })
}