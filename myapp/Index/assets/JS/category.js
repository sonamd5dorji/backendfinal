window.onload = function() {
    fetch('/categories')
      .then(response => response.json())
      .then(data => showCategories(data))
      .catch(error => {
        console.error('Error fetching categories:', error);
      });
  };
  
  function getFormData() {
    var formData = {
      cid: document.getElementById("cateid").value,
      catename: document.getElementById("catename").value
    };
    return formData;
  }
  
  function addCategory() {
    var data = getFormData();
    var cid = data.cid;
  
    fetch("/category", {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-type": "application/json;charset=UTF-8" }
    })
      .then(response1 => {
        if (response1.ok) {
          fetch("/category/" + cid)
            .then(response2 => response2.json())
            .then(data => showCategory(data));
        } else {
          throw new Error(response1.statusText);
        }
      })
      .catch(e => {
        alert(e);
      });
  
    resetForm();
  }
  
  function showCategory(data) {
    const category = JSON.parse(data);
    newRow(category);
  }
  
  function resetForm() {
    document.getElementById("cateid").value = "";
    document.getElementById("catename").value = "";
  }
  
  function showCategories(data) {
    const categories = JSON.parse(data);
    categories.forEach(category => {
      newRow(category);
    });
  }
  
  function newRow(categ) {
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.rows.length);
    var td = [];
  
    for (var i = 0; i < table.rows[0].cells.length; i++) {
      td[i] = row.insertCell(i);
    }
  
    td[0].innerHTML = categ.cid;
    td[1].innerHTML = categ.catename;
    td[2].innerHTML = '<input type="button" onclick="deleteCategory(this)" value="Delete">';
    td[3].innerHTML = '<input type="button" onclick="updateCategory(this)" value="Edit">';
  }
  
  function updateCategory(r) {
    var selectedRow = r.parentElement.parentElement;
    document.getElementById("cateid").value = selectedRow.cells[0].innerHTML;
    document.getElementById("catename").value = selectedRow.cells[1].innerHTML;
    var btn = document.getElementById("button-add");
    cid = selectedRow.cells[0].innerHTML;
    if (btn) {
      btn.innerHTML = "Update";
      btn.setAttribute("onclick", "update('" + cid + "')");
    }
  }
  
  function update(cid) {
    var newData = getFormData();
    fetch('/category/' + cid, {
      method: "PUT",
      body: JSON.stringify(newData),
      headers: { "Content-type": "application/json;charset=UTF-8" },
    })
      .then(res => {
        if (res.ok) {
          selectedRow.cells[0].innerHTML = newData.cid;
          selectedRow.cells[1].innerHTML = newData.catename;
          var btn = document.getElementById("button-add");
          if (btn) {
            btn.innerHTML = "Add";
            btn.setAttribute("onclick", "addCategory()");
            selectedRow = null;
            resetForm();
          } else {
            alert("Server: Update request error");
          }
        }
      });
  }
  
  function deleteCategory(r) {
    if (confirm("Are you sure you want to delete this category?")) {
      var selectedRow = r.parentElement.parentElement;
      var cid = selectedRow.cells[0].innerHTML;
  
      fetch("/category/" + cid, {
        method: "DELETE",
        headers: { "Content-type": "application/json;charset=UTF-8" }
      })
        .then(() => {
          selectedRow.remove();
        })
        .catch(error => {
          console.error('Error deleting category:', error);
        });
    }
  }
  