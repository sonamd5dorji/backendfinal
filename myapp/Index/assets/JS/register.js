function signUp(){
    // data to be sent to the POST request
    var _data = {
    fname : document.getElementById("fname").value,
    lname : document.getElementById("lname").value,
    cid : document.getElementById("CID").value,
    password : document.getElementById("pw1").value,
    pw : document.getElementById("pw2").value
    }

    if (_data.password !== _data.pw) {
      var popup = document.createElement("div");
      popup.innerHTML = "PASSWORD doesn't match!";
      popup.style.backgroundColor = "blue";
      popup.style.color = "white";
      popup.style.padding = "10px";
      popup.style.position = "fixed";
      popup.style.top = "50%";
      popup.style.left = "50%";
      popup.style.transform = "translate(-50%, -50%)";
      popup.style.zIndex = "9999";
      document.body.appendChild(popup);
      return;
    }
    fetch('/signup', {
    method: "POST",
    body: JSON.stringify(_data),
    headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => {
    if (response.status == 201) {
    // console.log("logged in")
    console.log(_data);
    window.open("login.html", "_self")
    }
    })
}    



let isValid = false;
let passwordsMatch = false;

function validateForm() {
	// using constrain API
	isValid = form.checkValidity();
	// style main message for an error

	if (!isValid) {
		message.textContent = "Fill out all fields";
		messageContainer.style.backgroundColor = "#ffd4d4";
		message.style.color = "#ff8979";
		// return : to stop further check
		return;
	}
	// check if passwords match
	if (password.value === pw.value) {
		passwordsMatch = true;
		password.style.borderColor = "#79ff8f";
		pw.style.borderColor = "#79ff8f";
	} else {
		passwordsMatch = false;
		message.textContent = "Match passwords";
		messageContainer.style.backgroundColor = "#ffd4d4";
		message.style.color = "#ff8979";
		password.style.borderColor = "#ff8979";
		pw.style.borderColor = "#ff8979";
		// return : to stop further check
		return;
	}
	//   if form is valid and passwords match
	if (isValid && passwordsMatch) {
		passwordsMatch = true;
		message.textContent = "Successfully Register";
		messageContainer.style.backgroundColor = "#deffe4";
		message.style.color = "#006b12";
		password.style.borderColor = "#79ff8f";
		pw.style.borderColor = "#79ff8f";
		form.style.display = "none";
		h1.textContent = `You're in!!!`;
		window.open("login.html", "_self");
	}
}

function storeFormData() {
	const user = {
		name: form.name.value,
		email: form.email.value,
		password: form.password.value
	};
	// do something with user data
	console.log(user);
}

function proccessFormData(e) {
	e.preventDefault();

	//   validate form
	validateForm();

	//   submit if valid
	if (isValid && passwordsMatch) {
		storeFormData();
	}
}

form.addEventListener("submit", proccessFormData);
