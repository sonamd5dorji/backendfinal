package model

import (
	"recipelab/myapp/DataStore/postgres"
)

type Recipe struct {
	RecipeID    int
	CID         string `json:"CID"`
	RecipeName  string `json:"recipeName"`
	Instruction string `json:"instruction"`
	Image       string `json:"image"`
}

const (
	queryInsertRecipe = "INSERT INTO recipes (CID,RecipeName,Instruction,Image) VALUES ($1,$2,$3,$4) RETURNING RecipeID;"
	queryGetRecipe    = "SELECT * FROM recipes WHERE  RecipeID=$1"
	queryGetAllRecipe = "SELECT * FROM recipes WHERE  UserEmail=$1"
	queryDeleteRecipe = "DELETE FROM recipes WHERE RecipeID=$1 RETURNING RecipeID;"
	queryUpdateRecipe = "UPDATE recipes SET RecipeID=$1, CID=$2,RecipeName=$3,Description=$4,Ingredient=$5,Instruction=$6,Image=$7 WHERE RecipeID=$8 RETURNING RecipeID;"
)

func (r *Recipe) AddRecipe() error {
	return postgres.Db.QueryRow(queryInsertRecipe, r.CID, r.RecipeName, r.Instruction, r.Image).Scan(&r.RecipeID)
}

func (r *Recipe) GetRecipe() error {
	return postgres.Db.QueryRow(queryGetRecipe, r.RecipeID).Scan(&r.RecipeID, &r.CID, &r.RecipeName, &r.Instruction, &r.Image)
}

func GetAllRecipe(email string) ([]Recipe, error) {
	rows, getErr := postgres.Db.Query(queryGetAllRecipe, email)
	if getErr != nil {
		return nil, getErr
	}

	recipes := []Recipe{}

	for rows.Next() {
		var r Recipe
		dbErr := rows.Scan(&r.RecipeID, &r.CID, &r.RecipeName, &r.Instruction, &r.Image)
		if dbErr != nil {
			return nil, dbErr
		}

		recipes = append(recipes, r)
	}
	rows.Close()
	return recipes, nil
}

func (r *Recipe) Delete() error {
	err := postgres.Db.QueryRow(queryDeleteRecipe, r.RecipeID).Scan(&r.RecipeID)
	return err
}

func (r *Recipe) Update(rid int) error {
	return postgres.Db.QueryRow(queryUpdateRecipe, r.RecipeID, r.CID, r.RecipeName, r.Instruction, r.Image, rid).Scan(&r.RecipeID)
}
