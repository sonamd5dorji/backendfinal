package model

import (
	"recipelab/myapp/DataStore/postgres"
)

type Admin struct {
	Firstname string `json:"fname"`
	Lastname  string `json:"lname"`
	CID       string `json:"cid"`
	Password  string `json:"password"`
}

const queryInsertAdmin = "INSERT INTO admin(firstname,lastname,cid,password) VALUES($1, $2, $3, $4);"

const queryGetAdmin = "SELECT CID, password FROM admin WHERE CID=$1 and password=$2;"

const queryGetTotalAdmin = "SELECT COUNT(*) FROM admin;"

const queryGetAdminDetails = "select cid, firstname, lastname, password FROM admin WHERE cid=$1;"

const queryUpdateAdmin = "UPDATE admin SET cid=$1, firstname=$2, lastname=$3 WHERE cid=$4 RETURNING cid"

func (c *Admin) Read() error {
	row := postgres.Db.QueryRow(queryGetAdminDetails, c.CID)
	err := row.Scan(&c.CID, &c.Firstname, &c.Lastname, &c.Password)
	return err

}

func (c *Admin) Update(old_coId string) error {
	err := postgres.Db.QueryRow(queryUpdateAdmin, c.CID, c.Firstname, c.Lastname, old_coId).Scan(&c.CID)
	return err
}

func GettotalAD() (int, error) {
	var count int
	err := postgres.Db.QueryRow(queryGetTotalAdmin).Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (adm *Admin) Create() error {
	_, err := postgres.Db.Exec(queryInsertAdmin, adm.Firstname, adm.Lastname, adm.CID, adm.Password)
	return err
}

func (adm *Admin) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.CID,
		adm.Password).Scan(&adm.CID, &adm.Password)
}
