create table USERTABLE(
    UID SERIAL,
    FName varchar(45) NOT NULL,
    LName varchar(45) NOT NULL,
    Email varchar(50) not null,
    Password varchar(50) not null,
    Uimage text default NULL,
    primary key (UID),
    unique(Email)
  );
create table Admin(
    adminid int not null primary key,
    adminname varchar (50) not null,
    password varchar(50) not null
)
create table Category(
    id int not null,
    category varchar(25) not null  primary key,
    UNIQUE(id)
)


create table recipe_master(
    recipeid serial primary key,
    useremail varchar(45) not null,
    recipename varchar(40) not null,
    description varchar(1000) null,
    recipeimg text default null,
    CONSTRAINT usertable_fk FOREIGN KEY (useremail) REFERENCES USERTABLE(Email) ON DELETE 
    CASCADE ON UPDATE CASCADE
);


create table report_recipe(
    report_id serial ,
    recipeid int not null,
    PRIMARY KEY (report_id),
    CONSTRAINT recipe_master_fk FOREIGN KEY (recipeid) REFERENCES recipe_master(recipeid) ON DELETE 
    CASCADE ON UPDATE CASCADE
)